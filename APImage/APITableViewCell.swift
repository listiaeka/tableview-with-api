//
//  APITableViewCell.swift
//  APImage
//
//  Created by Listia on 8/14/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import UIKit

class APITableViewCell: UITableViewCell {

    @IBOutlet weak var imageAPI: UIImageView!
    @IBOutlet weak var titleAPI: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
