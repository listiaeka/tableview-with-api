//
//  Post.swift
//  APImage
//
//  Created by Listia on 8/14/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import Foundation
import ObjectMapper

struct PostResponse: Mappable {
    
    var data: [Post]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
    }
}

struct Post: Mappable {
    
    var id: Int?
    var title: String?
    var url: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id      <- map["id"]
        title   <- map["title"]
        url     <- map["url"]
    }
    
}
