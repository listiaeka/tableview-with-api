//
//  ViewController.swift
//  APImage
//
//  Created by Listia on 8/14/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import UIKit
import Kingfisher
import SVProgressHUD
import SwiftPhotoGallery

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var data = [Post]()
    var postViewModel = PostViewModel()
    var selecData = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
            
        self.getData()
       
    }
    
    func getData() {
        
        SVProgressHUD.show(withStatus: "Memuat...")
        self.postViewModel.getPost(onSuccess: { postData in
            
            self.data = postData
            self.tableView.reloadData()
            SVProgressHUD.dismiss()
            
        }) { error in
            print("Errornya apa? \(error)")
            SVProgressHUD.dismiss()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail" {
            let detail = segue.destination as! DetailViewController
            detail.idPOST = self.data[self.selecData].id!
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? APITableViewCell else {
            return UITableViewCell()
        }
        
        let dataAPI = data[indexPath.row]
        
        let url = URL(string: dataAPI.url!)
        cell.titleAPI.text = dataAPI.title
        cell.imageAPI.kf.setImage(with: url)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selecData = indexPath.row
        self.performSegue(withIdentifier: "detail", sender: self)
    }
    
}                                                        
