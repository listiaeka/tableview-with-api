//
//  Service.swift
//  APImage
//
//  Created by Listia on 8/14/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

struct Service {
    
    static func getPost(onSuccess: @escaping (_ data: [Post]) -> Void, onFailure: @escaping (_ error: Error) -> Void) {
        let urlAPI = "https://jsonplaceholder.typicode.com/photos"
        
        Alamofire.request(urlAPI, method: .get, encoding: JSONEncoding.default).responseJSON { response
            in
            
            switch response.result {
                
            case .success:
                let responsnya = response.result.value
                let mapping = Mapper<Post>().mapArray(JSONArray: responsnya as! [[String : Any]])
                onSuccess((mapping))
                //print("Data: \(responsnya)")
                
            case .failure(let error):
                onFailure(error)
                //print("Errornya: \(error)")
                
            }
        }
    }
    
}
