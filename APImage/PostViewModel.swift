//
//  PostViewModel.swift
//  APImage
//
//  Created by Listia on 8/14/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

struct PostViewModel {
    
    func getPost(onSuccess: @escaping (_ data: [Post]) -> Void, onFailure: @escaping (_ error: Error) -> Void) {
        
        let apiURL = APIManager.baseURL
        Alamofire.request(apiURL, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                let json = response.result.value
                let JSONmap = Mapper<Post>().mapArray(JSONArray: json as! [[String : Any]])
                onSuccess(JSONmap)
            case .failure(let error):
                onFailure(error)
            }
        }
        
    }
    
    func detailPOST(idPOST: Int, onSuccess: @escaping (_ data: Post) -> Void, onFailure: @escaping (_ error: Error) -> Void) {
        let api = APIManager.baseURL + "/\(idPOST)"
        Alamofire.request(api, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            
            switch response.result {
                
            case .success:
                let getJSON = response.result.value
                let json = Mapper<Post>().map(JSON: getJSON as! [String : Any])
                onSuccess(json!)
                
            case .failure(let error):
                onFailure(error)
                
            }
        }
    }
}
