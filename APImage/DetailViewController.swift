//
//  DetailViewController.swift
//  APImage
//
//  Created by Listia on 8/15/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import UIKit
import Kingfisher
import SVProgressHUD
import RIGImageGallery

class DetailViewController: UIViewController {

    @IBOutlet weak var gambar: UIImageView!
    @IBOutlet weak var judul: UILabel!
    
    
    var postViewModel = PostViewModel()
    var idPOST = 0
    
    var urlGambar: String?
    
    
    fileprivate let imageSession = URLSession(configuration: .default)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        gambar.isUserInteractionEnabled = true
        gambar.addGestureRecognizer(tapGestureRecognizer)
        
        self.detail()

    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {

        print("aksi jalan")
        let photoViewController = createPhotoGallery()
        photoViewController.dismissHandler = dismissPhotoViewer
        photoViewController.traitCollectionChangeHandler = traitCollectionChangeHandler
        photoViewController.countUpdateHandler = updateCount
        let navigationController = UINavigationController(rootViewController: photoViewController)
        present(navigationController, animated: true, completion: nil)
    
    }
    
    
    func dismissPhotoViewer(_ :RIGImageGalleryViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func actionButtonHandler(_: RIGImageGalleryViewController, galleryItem: RIGImageGalleryItem) {
    }
    
    func updateCount(_ gallery: RIGImageGalleryViewController, position: Int, total: Int) {
        gallery.countLabel.text = "\(position + 1) of \(total)"
    }
    
    func traitCollectionChangeHandler(_ photoView: RIGImageGalleryViewController) {
        let isPhone = UITraitCollection(userInterfaceIdiom: .phone)
        let isCompact = UITraitCollection(verticalSizeClass: .compact)
        let allTraits = UITraitCollection(traitsFrom: [isPhone, isCompact])
        photoView.doneButton = photoView.traitCollection.containsTraits(in: allTraits) ? nil : UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
    }
    
    
    func createPhotoGallery() -> RIGImageGalleryViewController {
        
        let urls: [URL] = [
           self.urlGambar!
            ].flatMap(URL.init(string:))
        
        let rigItems: [RIGImageGalleryItem] = urls.map { _ in
            RIGImageGalleryItem(placeholderImage: UIImage(named: "placeholder") ?? UIImage(),
                                isLoading: true)
        }
        
        let rigController = RIGImageGalleryViewController(images: rigItems)
        
        for (index, URL) in urls.enumerated() {
            let request = imageSession.dataTask(with: URLRequest(url: URL)) { [weak rigController] data, _, error in
                if let image = data.flatMap(UIImage.init), error == nil {
                    rigController?.images[index].image = image
                    rigController?.images[index].isLoading = false
                }
            }
            request.resume()
        }
        
        return rigController
    }

    
    func detail() {
        SVProgressHUD.show(withStatus: "Memuat...")
        self.postViewModel.detailPOST(idPOST: idPOST, onSuccess: { data in
            
            print("Data: \(data)")
            SVProgressHUD.dismiss()
            
            let url = URL(string: data.url!)
            self.gambar.kf.setImage(with: url)
            self.judul.text = data.title
            self.urlGambar = data.url
            
        }) { error in
            
            print("Errornya: \(error)")
            SVProgressHUD.dismiss()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

